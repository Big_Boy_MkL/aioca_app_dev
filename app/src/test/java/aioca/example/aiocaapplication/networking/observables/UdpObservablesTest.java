package aioca.example.aiocaapplication.networking.observables;

import org.junit.After;
import org.junit.Test;

import aioca.example.aiocaapplication.util.MyConversions;

import static org.junit.Assert.*;

public class UdpObservablesTest {

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void convertSignedByteToUnsignedIntTest() {

        assertEquals(1,MyConversions.convertSignedByteToUnsignedInt((byte) 1));

        assertEquals(2,MyConversions.convertSignedByteToUnsignedInt((byte) 2));

        assertEquals(127,MyConversions.convertSignedByteToUnsignedInt((byte) 127));

        assertEquals(128,MyConversions.convertSignedByteToUnsignedInt((byte) -128));

        assertEquals(254,MyConversions.convertSignedByteToUnsignedInt((byte) -2));

        assertEquals(255,MyConversions.convertSignedByteToUnsignedInt((byte) -1));

        assertEquals(44,MyConversions.convertSignedByteToUnsignedInt((byte) 300));

    }


    @Test
    public void getImageSizeFromByteArrayHeaderTest(){

        byte START_BYTE = 0x01;
        byte COMMAND_TYPE = -1;
        byte SEND_BYTE = 0x04;

        assertEquals(-1, MyConversions.getImageSizeFromByteArrayHeader(
                new byte[]{START_BYTE, COMMAND_TYPE, -1, -1, -1, -1, SEND_BYTE}));

        assertEquals(6969, MyConversions.getImageSizeFromByteArrayHeader(
                new byte[]{START_BYTE, COMMAND_TYPE, 0, 0, 27, 57, SEND_BYTE}));


    }

}