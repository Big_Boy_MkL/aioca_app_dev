package aioca.example.aiocaapplication.services;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.text.format.Formatter;

import androidx.test.platform.app.InstrumentationRegistry;

import org.junit.Test;

import java.net.UnknownHostException;

import static aioca.example.aiocaapplication.networking.util.IpTools.getSmartPhonesIp;
import static org.junit.Assert.*;

public class AiocaNetworkServiceTest {

    @Test
    public void testIpFormatter() throws UnknownHostException {

        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        WifiManager wifiManager = (WifiManager)appContext.getSystemService(Context.WIFI_SERVICE);

        // Old Method to Text,
        // This might actually Be wrong if Androids decides to make it stop working.. ¯\_(ツ)_/¯
        String expected_host = Formatter.formatIpAddress(wifiManager.getConnectionInfo().getIpAddress());

        assertEquals(expected_host, getSmartPhonesIp(wifiManager).getHostAddress());

    }

}