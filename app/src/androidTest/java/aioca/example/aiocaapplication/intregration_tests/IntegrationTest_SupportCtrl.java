package aioca.example.aiocaapplication.intregration_tests;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.wifi.WifiManager;
import android.os.IBinder;

import androidx.test.platform.app.InstrumentationRegistry;

import org.junit.Test;

import java.io.IOException;
import java.net.DatagramPacket;
import java.util.concurrent.CountDownLatch;

import aioca.example.aiocaapplication.networking.controllers.UdpController;
import aioca.example.aiocaapplication.networking.util.IpTools;
import aioca.example.aiocaapplication.services.AiocaNetworkService;
import aioca.example.aiocaapplication.util.ServiceHandle;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.disposables.Disposable;

import static aioca.example.aiocaapplication.util.MyConversions.convertSignedByteToUnsignedInt;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class IntegrationTest_SupportCtrl {

    Context context = InstrumentationRegistry.getInstrumentation().getTargetContext();
    ServiceHandle serviceHandle = ServiceHandle.getInstance(context);

    private void startServiceTest(){
        Intent serviceIntent = new Intent(context, AiocaNetworkService.class);
        context.startService(serviceIntent);
    }

    private void stopServiceTest(){
        Intent AiocaServiceIntent = new Intent(context, AiocaNetworkService.class);
        context.stopService(AiocaServiceIntent);
    }

    @Test
    public void test_UdpPackageReceived(){
        final CountDownLatch onServiceConnectedSignal = new CountDownLatch(1);
        WifiManager wifiManager = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);
        startServiceTest();

        ServiceConnection serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                AiocaNetworkService aiocaNetworkService = ((AiocaNetworkService.AiocaNetworkServiceBinder) iBinder).getService();
                aiocaNetworkService.wiFiController.registerToNetwork();

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                int length = 64;
                byte[] data = new byte[length];
                DatagramPacket datagramPacket = new DatagramPacket(data, length);
                aiocaNetworkService.udpController = UdpController.getInstance();
                try {
                    aiocaNetworkService.udpController.Setup(aiocaNetworkService.wiFiController.getConnectedNetwork(), IpTools.getSmartPhonesIp(wifiManager),2000);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                aiocaNetworkService.startVideoFeed(new SingleObserver<DatagramPacket>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onSuccess(@NonNull DatagramPacket datagramPacket) {
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }
                });

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        int lenReceived = 0;
                        int counter = 0;

                        while (lenReceived != 7) {
                            try {
                                aiocaNetworkService.udpController.Read(datagramPacket);
                            } catch (IOException e) {
                                e.printStackTrace();
                                fail();
                                onServiceConnectedSignal.countDown();
                            }

                            lenReceived = datagramPacket.getLength();

                            if (counter > 50){
                                fail();
                            }
                        }

                        byte[] recevied = datagramPacket.getData();
                        int bufimagesize = 0;
                        bufimagesize += (convertSignedByteToUnsignedInt(recevied[2]) << 24);
                        bufimagesize += (convertSignedByteToUnsignedInt(recevied[3]) << 16);
                        bufimagesize += (convertSignedByteToUnsignedInt(recevied[4]) << 8);
                        bufimagesize += (convertSignedByteToUnsignedInt(recevied[5]));

                        // Check if start and stop are in msg
                        assertTrue(recevied[0] == 0x01);
                        assertTrue(recevied[6] == 0x04);

                        // Image size should be more than 1000 bytes
                        assertTrue(bufimagesize > 1000);

                        onServiceConnectedSignal.countDown();
                    }
                }).start();
            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {
            }
        };
        serviceHandle.bindToService(serviceConnection);

        try{
            onServiceConnectedSignal.await();
            stopServiceTest();
        }catch (InterruptedException e){
            fail();
        }
    }

    @Test
    public void test_KeepAliveCommand_ACK(){
        final CountDownLatch onServiceConnectedSignal = new CountDownLatch(1);
        WifiManager wifiManager = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);
        startServiceTest();

        ServiceConnection serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                AiocaNetworkService aiocaNetworkService = ((AiocaNetworkService.AiocaNetworkServiceBinder) iBinder).getService();
                aiocaNetworkService.wiFiController.registerToNetwork();

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                aiocaNetworkService.startSendKeepAlive(new SingleObserver<DatagramPacket>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onSuccess(@NonNull DatagramPacket datagramPacket) {
                        byte[] testPackage = new byte[64];
                        testPackage[0] = 1;
                        testPackage[1] = 6;
                        testPackage[2] = 4;
                        assertArrayEquals(datagramPacket.getData(), testPackage);
                        onServiceConnectedSignal.countDown();
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        onServiceConnectedSignal.countDown();
                    }
                });

            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {
            }
        };
        serviceHandle.bindToService(serviceConnection);

        try{
            onServiceConnectedSignal.await();
            stopServiceTest();
        }catch (InterruptedException e){
            fail();
        }
    }

    @Test
    public void test_startVideoFeedCommand_ACK(){
        final CountDownLatch onServiceConnectedSignal = new CountDownLatch(1);
        startServiceTest();

        ServiceConnection serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                AiocaNetworkService aiocaNetworkService = ((AiocaNetworkService.AiocaNetworkServiceBinder) iBinder).getService();
                aiocaNetworkService.wiFiController.registerToNetwork();

                aiocaNetworkService.startVideoFeed(new SingleObserver<DatagramPacket>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onSuccess(@NonNull DatagramPacket datagramPacket) {
                        byte[] testPackage = new byte[64];
                        testPackage[0] = 1;
                        testPackage[1] = 6;
                        testPackage[2] = 4;
                        assertArrayEquals(datagramPacket.getData(), testPackage);
                        onServiceConnectedSignal.countDown();
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }
                });
            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {
            }
        };
        serviceHandle.bindToService(serviceConnection);
        try{
            onServiceConnectedSignal.await();
            stopServiceTest();
        }catch (InterruptedException e){
            fail();
        }
    }

}
