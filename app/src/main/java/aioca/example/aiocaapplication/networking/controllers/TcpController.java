package aioca.example.aiocaapplication.networking.controllers;

import android.net.Network;
import android.util.Log;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.NoRouteToHostException;
import java.net.Socket;

import javax.net.SocketFactory;

public class TcpController implements ISocketController{

    private final static String     TAG = "TCPController";

    private Socket                  tcpSocket;
    private DataInputStream         tcpDataInputStream;
    private DataOutputStream        tcpDataOutputStream;

    public TcpController() {

    }

    /**
     * Will make a TCP Socket with the SocketFactory of the Network
     * @param network Network: Network to setup on
     * @param address InetAddress: InetAddress to Connect to
     * @param port Int: What Port Socket should be connected on
     * @throws IOException Exception if CreateSocket Fails or getOutputStream or getInputStream
     */
    @Override
    public void Setup(Network network, InetAddress address, int port) throws IOException {
        // Reset sockets
        if (tcpSocket != null)
        {
            tcpSocket.close();
        }
        tcpSocket = null;

        // Will create sockets on the provided network Parameter
        Log.d(TAG, "setupTCPSocket: Creating Socket Factory");
        SocketFactory socketFactory = network.getSocketFactory();
        Log.d(TAG, "setupTCPSocket: Create Socket");
        tcpSocket = socketFactory.createSocket(address, port);

        // SetUp Input and Output Streams for the TCP Port
        tcpDataOutputStream = new DataOutputStream(tcpSocket.getOutputStream());
        tcpDataInputStream = new DataInputStream(tcpSocket.getInputStream());
    }


    /**
     * Will Close the Sockets and destroy the socket as well as destroy
     * the input and output stream
     * @throws IOException if isClosed is null
     */
    @Override
    public void Destroy() throws IOException {
        Log.d(TAG, "destroySockets: Destroying TCP Sockets");
        if (tcpSocket != null && !tcpSocket.isClosed()){
            tcpSocket.close();
        }
        /**
         * These might not be needed as they are destroyed when socket is closed above
         */
        tcpSocket = null;
    }

    /**
     * Will Read on the tcpDataInputStream and Input data into DatagramPacket
     * @param datagramPacket DatagramPacket: Packet to put data into
     * @return Length of read data
     * @throws IOException if Read fails
     */
    @Override
    public int Read(DatagramPacket datagramPacket) throws IOException {
        return tcpDataInputStream.read(datagramPacket.getData());
    }

    /**
     * Will Write with the tcpDataOutputStream and Input data into datagramPacket
     * @param datagramPacket DatagramPacket: Packet to assign data to
     * @throws IOException If Write fails
     */
    @Override
    public void Write(DatagramPacket datagramPacket) throws IOException {
        tcpDataOutputStream.write(datagramPacket.getData());
    }

    /**
     * @return Boolean, Will return true if sockets is alive, else false
     */
    @Override
    public boolean IsAlive() {
        if (tcpSocket != null){
            return tcpSocket.isConnected();
        }
        return false;
    }
}
