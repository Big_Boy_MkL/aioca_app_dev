package aioca.example.aiocaapplication.networking.observables;

import android.util.Log;

import java.net.DatagramPacket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import aioca.example.aiocaapplication.services.AiocaNetworkService;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.disposables.Disposable;

import static aioca.example.aiocaapplication.util.MyConversions.convertSignedByteToUnsignedInt;
import static aioca.example.aiocaapplication.util.MyConversions.getImageSizeFromByteArrayHeader;

/**
 * http://reactivex.io/RxJava/javadoc/io/reactivex/Observable.html
 */
public class UdpObservables {

    private AiocaNetworkService         aiocaNetworkService;
    private boolean                     setStop = false;
    private static int                  BUFFER_SIZE = 6000;
    private static String               TAG = "UdpObservable";

    private ScheduledExecutorService    scheduleTaskExecutor;


    public UdpObservables(AiocaNetworkService aiocaNetworkService) {
        this.aiocaNetworkService = aiocaNetworkService;
    }

    /**
     * RxJava Observable where onNext will be called with an ArrayList<byte[]>
     * Returns and RXJava Observable that will create UDP Sockets and continue reading on it
     * until the the read times out or the thread is asked to show down
     * @return Observable<ArrayList<byte[]>>
     */
    public Observable<ArrayList<byte[]>> getVideoFeed(){
        return Observable.create(emitter -> {
            setStop = false;
            /**
             * Start Scheduled Task Executor to send KeepAlive
             */
            Log.d(TAG, "getVideoFeed: Starting Keep Alive");
            scheduleTaskExecutor = Executors.newSingleThreadScheduledExecutor();
            scheduleTaskExecutor.scheduleAtFixedRate(() -> aiocaNetworkService.startSendKeepAlive(new SingleObserver<DatagramPacket>() {
                        @Override
                        public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {
                        }

                        @Override
                        public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull DatagramPacket data) {
                            Log.d(TAG, "onSuccess: Keep Alive Succeded");
                        }

                        @Override
                        public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                            Log.d(TAG, "onError: Keep Alive Request Failed", e);
                        }
                    }),
                    2,2, TimeUnit.SECONDS);
            Log.d(TAG, "getVideoFeed: Executor Started");

            while (!setStop){
                /**
                 * Reset Buffers and Sizes
                 */
                byte[] buf = new byte[BUFFER_SIZE];
                int sizeRead = 0;
                int imageSize = 0;
                DatagramPacket datagramPacket = new DatagramPacket(buf, buf.length);

                /**
                 * Keep reading UDP until header is received. UDPHeader is only
                 */
                int read_length = 0;
                do {
                    try {
                        read_length = aiocaNetworkService.readUdp(datagramPacket);
                    }catch (SocketTimeoutException e){
                        Log.d(TAG, "getVideoFeed: RECEIVE_HEADER: Timeout in receive");
                        setStop = true;
                        break;
                    }
                } while (read_length != 7 && !setStop);


                /**
                 * Decode the UDP Message Header
                 */
                if (buf[0] == 0x01 && buf[6] == 0x04 && convertSignedByteToUnsignedInt(buf[1]) == 0xff && !setStop) {
                    imageSize = getImageSizeFromByteArrayHeader(buf);

                    /**
                     * Read Image
                     */
                    ArrayList<byte[]> bufArrList = new ArrayList<>();
                    while (sizeRead < imageSize && !setStop){
                        try {
                            aiocaNetworkService.readUdp(datagramPacket);
                        }catch (SocketTimeoutException e){
                            Log.d(TAG, "getVideoFeed: RECEIVE_IMAGE: Timeout in receive");
                            setStop = true;
                            break;
                        }
                        bufArrList.add(datagramPacket.getData().clone());
                        sizeRead += datagramPacket.getLength();
                    }

                    /**
                     * If Image is not corrupted then send it to Observer
                     */
                    if (imageSize == sizeRead && !setStop) {
                        ArrayList<byte[]> toSend = new ArrayList<>(bufArrList);
                        emitter.onNext(toSend);

                    } else {
                        // Part that might get us back on track when we receives larger packets than expected
                        Log.w(TAG, "getVideoFeed: Image sizes are not correct");
                    }
                }
            }
            /**
             * Make Sure to close FPS Counter in the Observer
             * shutdownNow can be used to kill taskExecutor.
             * Shutdown will only stop executing further tasks
             */
            Log.w(TAG, "getVideoFeed: Closing down Keep Alive");
            shutdownKeepAlive();
            Log.d(TAG, "getVideoFeed: Destroying Sockets!");
            aiocaNetworkService.udpController.Destroy();
            aiocaNetworkService.udpController = null;
            Log.d(TAG, "getVideoFeed: UDP Observable closing Down");
            emitter.onComplete();
        });
    }

    /**
     * Close down the Keep Alive Executor sending TCP Keep Alive Request Each Second
     */
    public void shutdownKeepAlive(){
        if (scheduleTaskExecutor != null && !scheduleTaskExecutor.isShutdown()){
            scheduleTaskExecutor.shutdown();
        }
    }

    /**
     * Close the Udp Reading Thread
     * Udp Thread will clean up by itself on exit
     */
    public void setThreadForClose(){
        setStop = true;
    }

}
