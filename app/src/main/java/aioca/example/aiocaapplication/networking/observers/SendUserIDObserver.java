package aioca.example.aiocaapplication.networking.observers;

import android.util.Log;

import java.net.DatagramPacket;

import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.disposables.Disposable;

public class SendUserIDObserver implements SingleObserver<DatagramPacket> {

    private final static String     TAG = "SendUserIDObserver";

    CallBackInterface               cb_;

    public interface CallBackInterface {
        void CallBackSuccess(boolean loggedIn);
        void CallBackError();
    }

    /**
     * SendUserIDObserver that will used to observe on when a request to login the user on the
     * MainCtrl
     * @param cb Callback to run when message is received
     */
    public SendUserIDObserver(CallBackInterface cb) {
        this.cb_ = cb;
    }

    /**
     * Called when something subscribes to the observable.
     * @param d Disposable
     */
    @Override
    public void onSubscribe(@NonNull Disposable d) {
    }

    /**
     * Called when the TCP request have been sent successfully.
     * @param data A DatagramPacket containing the response from the TCP request.
     */
    @Override
    public void onSuccess(@NonNull DatagramPacket data) {
        Log.d(TAG, "onSuccess: SendUserIDObserver");

        byte[] received = data.getData();
        if (received[0] == 0x01 && received[1] == 0x06 && received[2] == 0x04)
            cb_.CallBackSuccess(true);
        else
            cb_.CallBackSuccess(false);
    }

    /**
     * Called if an error occurs in the transmission.
     * @param e Throwable
     */
    @Override
    public void onError(@NonNull Throwable e) {
        cb_.CallBackError();
    }
}
