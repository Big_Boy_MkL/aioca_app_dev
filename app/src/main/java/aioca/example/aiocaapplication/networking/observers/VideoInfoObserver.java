package aioca.example.aiocaapplication.networking.observers;

import java.net.DatagramPacket;

import aioca.example.aiocaapplication.videoinfo.models.TimeStamp;
import aioca.example.aiocaapplication.videoinfo.models.VideoInfo;
import aioca.example.aiocaapplication.viewmodels.SavedViewModel;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.disposables.Disposable;

import static aioca.example.aiocaapplication.util.MyConversions.convertSignedByteToUnsignedInt;

public class VideoInfoObserver implements SingleObserver<DatagramPacket> {

    private int index_;
    SavedViewModel savedViewModel;

    public VideoInfoObserver(int index, SavedViewModel savedViewModel) {
        this.index_ = index;
        this.savedViewModel = savedViewModel;
    }

    /**
     * Called when something subscribes to the observable.
     * @param d Disposable
     */
    @Override
    public void onSubscribe(@NonNull Disposable d) {

    }

    /**
     * Called when the TCP request have been sent successfully - The receive data is put into
     * a VideoInfo class and the put into the database.
     * @param datagramPacket A DatagramPacket containing the response from the TCP request.
     */
    @Override
    public void onSuccess(@NonNull DatagramPacket datagramPacket) {
        byte[] receivedData = datagramPacket.getData();

        int startIdx_ = ConvertBytesToNum(receivedData, 1);
        int size_ = ConvertBytesToNum(receivedData, 5);
        int n_images = ConvertBytesToNum(receivedData, 9);

        TimeStamp timeStamp;
        if (receivedData[13] != 0 || receivedData[14] != 0 || receivedData[15] != 0 || receivedData[16] != 0 || receivedData[17] != 0){
            timeStamp = new TimeStamp(convertSignedByteToUnsignedInt(receivedData[13]) + 1970,
                    convertSignedByteToUnsignedInt(receivedData[14]),
                    convertSignedByteToUnsignedInt(receivedData[15]),
                    convertSignedByteToUnsignedInt(receivedData[16]),
                    convertSignedByteToUnsignedInt(receivedData[17]));
        }
        else{
            timeStamp = new TimeStamp();
        }

        VideoInfo videoInfo = new VideoInfo(startIdx_, size_, n_images, timeStamp);
        savedViewModel.insertSetting(videoInfo);
    }

    /**
     * Called if an error occurs in the transmission.
     * @param e Throwable
     */
    @Override
    public void onError(@NonNull Throwable e) {

    }

    private int ConvertBytesToNum(byte[] array, int offset) {
        int temp = 0;
        temp += (convertSignedByteToUnsignedInt(array[offset + 3]) << 24);
        temp += (convertSignedByteToUnsignedInt(array[offset + 2]) << 16);
        temp += (convertSignedByteToUnsignedInt(array[offset + 1]) << 8);
        temp += (convertSignedByteToUnsignedInt(array[offset]));
        return temp;
    }
}