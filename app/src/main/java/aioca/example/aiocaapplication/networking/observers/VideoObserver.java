package aioca.example.aiocaapplication.networking.observers;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.SystemClock;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.util.ArrayUtils;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;


import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;

public class VideoObserver implements Observer<ArrayList<byte[]>> {


    ImageView               imageView;
    TextView                fpsCounter;
    int                     imageCounter    = 0;

    // TODO Make it so that the FPS counter can be toggled in the user settings

    String                  fps;
    Timer                   timer;

    Activity                activity;

    public static String    TAG             = "VideoObservers";

    public VideoObserver(ImageView imageView, TextView fpsCounter, Activity activity) {
        this.imageView = imageView;
        this.fpsCounter = fpsCounter;
        this.activity = activity;
        fps = "0";
    }


    /**
     * Will Start a Thread that will update the UI with FPS Counter each 500 Ms
     * @param d Disposable
     */
    @Override
    public void onSubscribe(@NonNull Disposable d) {
        imageCounter = 0;
        long startMil = SystemClock.elapsedRealtime();

        timer = new Timer();
        timer.scheduleAtFixedRate( new TimerTask() {
            @Override
            public void run() {
                long elapsedMillis = SystemClock.elapsedRealtime() - startMil;
                fps = new DecimalFormat("#.##").format((double)imageCounter/(elapsedMillis/1000.0));

                //run on ui thread
                activity.runOnUiThread(() -> {
                    String stringBuilder = "FPS: " +
                            fps;
                    Log.d(TAG, "run: Total Frames: " + imageCounter);
                    fpsCounter.setText(stringBuilder);

                });

            }
        }, 0, 500 );

    }

    /**
     * Called whenever Observable calles onNext
     * @param arrList byte[] Image Data
     */
    @Override
    public void onNext(@NonNull ArrayList<byte[]> arrList) {
        /**
         * Convert the supplied ArrayList of Bytes to an Image
         */
        byte[] buf = new byte[0];
        for (byte[] bufByte: arrList) {
            buf = ArrayUtils.concatByteArrays(buf, bufByte);
        }
        /**
         * Decodes the Byte Array to an Bitmap Image and Displays on the Screen
         */
        try {
            Bitmap bitmap = BitmapFactory.decodeByteArray(buf, 0, buf.length);
            imageView.setImageBitmap(bitmap);
            imageCounter +=1;
        }catch (Exception e){
            e.printStackTrace();
            Log.d(TAG, "onNext: Yikes Image");
        }

    }

    /**
     * Closes down the Timer Task to stop updating FPS
     * @param e Throwable
     */
    @Override
    public void onError(@NonNull Throwable e) {
        timer.cancel();
    }

    /**
     * Closes down the Timer Task to stop updating FPS
     */
    @Override
    public void onComplete() {
        timer.cancel();
    }
}
