package aioca.example.aiocaapplication.networking.observers;

import android.content.Context;
import android.util.Log;
import android.widget.ProgressBar;

import java.net.DatagramPacket;
import java.util.Arrays;

import aioca.example.aiocaapplication.videoencoder.executor.EncodeVideoExecutor;
import aioca.example.aiocaapplication.videoencoder.InternalDataStorage;
import aioca.example.aiocaapplication.services.AiocaNetworkService;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.disposables.Disposable;

import static aioca.example.aiocaapplication.util.MyConversions.convertSignedByteToUnsignedInt;

public class RequestVideoObserver implements SingleObserver<DatagramPacket>  {

    private static String TAG = "RequestVideoObserver";

    AiocaNetworkService aiocaNetworkService;
    InternalDataStorage storage;
    EncodeVideoExecutor executor;
    CallBackInterface cb_;
    ProgressBar progressBar;


    public interface CallBackInterface {
        void CallBack();
    }

    ReceiveState state;

    int videoNumber;
    int n_frames = 0;
    int frameSize = 0;
    int currentFrameSize = 0;
    int n_framesCurrent = 0;
    int totalSize = 0;
    byte[] frameData;

    private enum ReceiveState {
        ReceiveVideoHeader,
        ReceiveImageHeader,
        ReceiveImageInfo,
    }

    public RequestVideoObserver(CallBackInterface callback, AiocaNetworkService aiocaNetworkService, Context context, int videoNumber, ProgressBar progressBar) {
        this.aiocaNetworkService = aiocaNetworkService;
        this.storage = new InternalDataStorage(context);
        this.executor = new EncodeVideoExecutor(context);
        this.videoNumber = videoNumber;
        this.state = ReceiveState.ReceiveVideoHeader;
        this.cb_ = callback;
        this.progressBar = progressBar;
    }

    /**
     * Called when something subscribes to the observable.
     * @param d Disposable
     */
    @Override
    public void onSubscribe(@NonNull Disposable d) {

    }

    /**
     * Called when the TCP request have been sent successfully - Stores information about
     * the video that its downloading. It will be sending new requests to the MainController
     * with itself as an observer in order to receive the entire video.
     * @param datagramPacket A DatagramPacket containing the response from the TCP request.
     */
    @Override
    public void onSuccess(@NonNull DatagramPacket datagramPacket) {
        byte[] receivedData = datagramPacket.getData();

        if (receivedData[0] == 0x02){
            cb_.CallBack();
            return;
        }

        if (state == ReceiveState.ReceiveVideoHeader){
            totalSize += (convertSignedByteToUnsignedInt(receivedData[8]) << 24);
            totalSize += (convertSignedByteToUnsignedInt(receivedData[7]) << 16);
            totalSize += (convertSignedByteToUnsignedInt(receivedData[6]) << 8);
            totalSize += (convertSignedByteToUnsignedInt(receivedData[5]));

            n_frames += (convertSignedByteToUnsignedInt(receivedData[12]) << 24);
            n_frames += (convertSignedByteToUnsignedInt(receivedData[11]) << 16);
            n_frames += (convertSignedByteToUnsignedInt(receivedData[10]) << 8);
            n_frames += (convertSignedByteToUnsignedInt(receivedData[9]));

            state = ReceiveState.ReceiveImageHeader;

            aiocaNetworkService.requestVideoInfo_ACK(this);
        }
        else if (state == ReceiveState.ReceiveImageHeader){
            if (n_framesCurrent >= n_frames){
                 // Done receiving
                 executor.ConvertVideo(0, n_frames);
                 cb_.CallBack();
                 return;
            }
            else{
                frameSize += (convertSignedByteToUnsignedInt(receivedData[4]) << 24);
                frameSize += (convertSignedByteToUnsignedInt(receivedData[3]) << 16);
                frameSize += (convertSignedByteToUnsignedInt(receivedData[2]) << 8);
                frameSize += (convertSignedByteToUnsignedInt(receivedData[1]));

                Log.d(TAG, "Framesize is: " + frameSize);
                frameData = new byte[frameSize];

                state = ReceiveState.ReceiveImageInfo;

                aiocaNetworkService.requestVideoInfo_ACK(this);
            }
        }
        else if (state == ReceiveState.ReceiveImageInfo) {
            if (receivedData[1] > 0){
                try {
                    System.arraycopy(Arrays.copyOfRange(receivedData, 2, receivedData[1] + 2), 0, frameData, currentFrameSize, receivedData[1]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                currentFrameSize += receivedData[1];

                if (currentFrameSize >= frameSize){
                    state = ReceiveState.ReceiveImageHeader;
                    storage.WriteFrameToInternalStorage(frameData, videoNumber, n_framesCurrent);

                    frameSize = 0;
                    currentFrameSize = 0;
                    n_framesCurrent++;
                    progressBar.setProgress((int)Math.round(((float)n_framesCurrent/(float)n_frames)*100.0));
                    Log.d(TAG, "Received image. Total num of frames " + n_framesCurrent);
                }
                aiocaNetworkService.requestVideoInfo_ACK(this);
            }
            else {
                aiocaNetworkService.requestVideoInfo_NAK(this);
            }
        }
    }

    @Override
    /**
     * Called if an error occurs in the transmission.
     * Calls callback to inform the Fragment of the error.
     * @param e Throwable
     */
    public void onError(@NonNull Throwable e) {
        cb_.CallBack();
        }
}