package aioca.example.aiocaapplication.networking.observers;

import java.net.DatagramPacket;

import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.disposables.Disposable;

public class NumOfSavedVidObserver implements SingleObserver<DatagramPacket>{

    CallBackInterface cb_;
    public interface CallBackInterface {
        void CallBack(int numOfVid);
    }

    public NumOfSavedVidObserver(CallBackInterface cb) {
        this.cb_ = cb;
    }

    /**
     * Called when something subscribes to the observable.
     * @param d Disposable
     */
    @Override
    public void onSubscribe(@NonNull Disposable d) {

    }

    /**
     * Called when the TCP request have been sent successfully - Calls the callback set in the
     * constructor.
     * @param datagramPacket A DatagramPacket containing the response from the TCP request.
     */
    @Override
    public void onSuccess(@NonNull DatagramPacket datagramPacket) {
        byte[] receivedData = datagramPacket.getData();
        if (receivedData[0] == 0x01)
            cb_.CallBack(datagramPacket.getData()[1]);
        else
            cb_.CallBack(0);
    }

    /**
     * Called if an error occurs in the transmission.
     * @param e Throwable
     */
    @Override
    public void onError(@NonNull Throwable e) {

    }
}