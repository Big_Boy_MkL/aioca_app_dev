package aioca.example.aiocaapplication.networking.controllers;

import android.net.Network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

import io.reactivex.rxjava3.exceptions.OnErrorNotImplementedException;

public class UdpController implements ISocketController{

    private static UdpController INSTANCE = null;
    private DatagramSocket          UdpSocket;

    private UdpController() {
    }


    public static UdpController getInstance(){
        if (INSTANCE == null){
            INSTANCE = new UdpController();
        }
        return INSTANCE;
    }


    /**
     * Will bind an UDP Socket to the Network Parameter
     * @param network Network: Network To setup socket on
     * @param address InetAddress: InetAddress of Address to make Socket on
     * @param port int: What port it should be done on
     * @throws IOException if Close or bindSocket fails
     */
    @Override
    public void Setup(Network network, InetAddress address, int port) throws IOException {
        if (UdpSocket != null && UdpSocket.isConnected()){
            UdpSocket.close();
            UdpSocket = null;
        }

        UdpSocket = new DatagramSocket(port, address);
        network.bindSocket(UdpSocket);
    }

    /**
     * Will disconnect and close sockets if active and destroy the socket
     */
    @Override
    public void Destroy() {
        if (UdpSocket != null){
            if (!UdpSocket.isConnected()){
                UdpSocket.disconnect();
            }
            if (!UdpSocket.isClosed()){
                UdpSocket.close();
            }
            UdpSocket = null;
        }
    }

    /**
     * Will receive data from the Socket and input into the Datagram Packet in the parameter
     * @param datagramPacket DatagramPacket: Packet to assign data to
     * @return int: length of read data.
     * @throws IOException If receive errors.
     */
    @Override
    public int Read(DatagramPacket datagramPacket) throws IOException {
        UdpSocket.receive(datagramPacket);
        return datagramPacket.getLength();
    }

    /**
     * Write data using the UDP Socket
     * Currently not implemented as Aioca wont use UDP for send on Smartphone
     * @param datagramPacket Where data should be from
     * @throws IOException if Write fails
     */
    @Override
    public void Write(DatagramPacket datagramPacket) throws IOException {
        throw new OnErrorNotImplementedException("Write not implemented", new Throwable());
    }

    /**
     * @return Boolean: Will return true if the socket is connected else false
     */
    @Override
    public boolean IsAlive()
    {
        if (UdpSocket != null){
            return UdpSocket.isConnected();
        }
        return false;
    }

    /**
     * @return Boolean: Will return the timeout current set on the IO operations for the UDP Socket
     * @throws SocketException if Socket is Null
     */
    public int getSoTimeout() throws SocketException {
        return UdpSocket.getSoTimeout();
    }

    /**
     * Will set the timeout on the IO Operations for the UDP Socket
     * @param timeout int: milliseconds to set timeout to
     * @throws SocketException if socket is null
     */
    public void setSoTimeout(int timeout) throws SocketException{
        UdpSocket.setSoTimeout(timeout);
    }
}
