package aioca.example.aiocaapplication.networking.observers.settingsobservers;

import java.net.DatagramPacket;

import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.disposables.Disposable;

public class FpsCounterSettingsObserver implements SingleObserver<DatagramPacket> {

    /**
     * Called when something subscribes to the observable.
     * @param d Disposable
     */
    @Override
    public void onSubscribe(@NonNull Disposable d) {

    }

    /**
     * Called when the TCP request have been sent successfully.
     * @param datagramPacket A DatagramPacket containing the response from the TCP request.
     */
    @Override
    public void onSuccess(@NonNull DatagramPacket datagramPacket) {


    }

    /**
     * Called if an error occurs in the transmission.
     * @param e Throwable
     */

    @Override
    public void onError(@NonNull Throwable e) {

    }
}
