package aioca.example.aiocaapplication.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import aioca.example.aiocaapplication.R;
import aioca.example.aiocaapplication.videoinfo.models.TimeStampConverters;
import aioca.example.aiocaapplication.videoinfo.models.VideoInfo;

/**
 * Inspiration Taken from:
 * https://developer.android.com/guide/topics/ui/layout/recyclerview
 */
public class VideoInfoAdapter extends RecyclerView.Adapter<VideoInfoAdapter.VideoInfoViewHolder> {

    private List<VideoInfo> videoInfoList;
    private VideoInfoAdapter.VideoInfoInterface videoInfoInterface;
    private Context mContext;

    public interface VideoInfoInterface{
         void onItemClick(int position, View v);
    }

    /**
     * Will create a VideoInfoViewHolder for context and add inflate the cardview and send the Click
     * Interface.
     * @param parent
     * @param viewType
     * @return
     */
    @NonNull
    @Override
    public VideoInfoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VideoInfoViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.videoinfo_card_view, parent, false),
                videoInfoInterface);
    }

    /**
     * What will happen for each card when it binds.
     * @param holder the specific VideoInfoViewHolder
     * @param position Position in the list
     */
    @Override
    public void onBindViewHolder(@NonNull VideoInfoViewHolder holder, int position) {

        String info_text = TimeStampConverters.fromTimeStampToString(videoInfoList.get(position)
                .getTimeStamp());

        holder.txtInfo.setText(info_text);

    }

    /**
     * Number of items in the list
     * @return number of items
     */
    @Override
    public int getItemCount() {
        if (videoInfoList != null){
            return videoInfoList.size();
        }
        return 0;
    }

    /**
     * Set the List of the adapter
     * @param videoInfoList List of VideoInfo objects
     */
    public void setVideoInfoList(List<VideoInfo> videoInfoList){
        this.videoInfoList = videoInfoList;
        notifyDataSetChanged();
    }

    /**
     * Constructor containing context
     * @param mContext context of application
     */
    public VideoInfoAdapter(Context mContext) {
        this.mContext = mContext;
    }

    /**
     * The clickInterface for the adapter
     * @param clickListener VideoInfoInterface for the adapter
     */
    public void setClickListener(VideoInfoInterface clickListener){
        videoInfoInterface = clickListener;
    }

    public static class VideoInfoViewHolder extends RecyclerView.ViewHolder{

        private VideoInfoInterface videoInfoInterface;
        private TextView txtInfo;

        /**
         * Create VideoInfoViewHolder containing all the needed object to change cards
         * @param itemView View of item
         * @param videoInfoInterface ClickInterface that is used.
         */
        public VideoInfoViewHolder(@NonNull View itemView, VideoInfoInterface videoInfoInterface) {
            super(itemView);
            this.videoInfoInterface = videoInfoInterface;
            txtInfo = itemView.findViewById(R.id.videoInfo_text);

            itemView.setOnClickListener(view -> {
                if (videoInfoInterface != null){
                    videoInfoInterface.onItemClick(getLayoutPosition(), view);
                }
            });


        }
    }
}

