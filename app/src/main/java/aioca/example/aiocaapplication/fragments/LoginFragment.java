package aioca.example.aiocaapplication.fragments;

import android.Manifest;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;

import android.os.IBinder;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;

import java.io.IOException;
import java.util.Objects;
import java.util.concurrent.ConcurrentLinkedQueue;

import aioca.example.aiocaapplication.R;
import aioca.example.aiocaapplication.networking.observers.SendUserIDObserver;
import aioca.example.aiocaapplication.networking.observers.settingsobservers.CameraSettingsObserver;
import aioca.example.aiocaapplication.networking.observers.settingsobservers.RoadStripAlertSettingsObserver;
import aioca.example.aiocaapplication.networking.observers.settingsobservers.SuddenBrakeSettingsObserver;
import aioca.example.aiocaapplication.services.AiocaNetworkService;
import aioca.example.aiocaapplication.databinding.FragmentLoginBinding;
import aioca.example.aiocaapplication.settings.AiocaSettings;
import aioca.example.aiocaapplication.util.ServiceHandle;
import aioca.example.aiocaapplication.viewmodels.LoginViewModel;
import aioca.example.aiocaapplication.viewmodels.SettingsViewModel;

public class LoginFragment extends Fragment {

    private static String               TAG = "LoginFragment";

    private FragmentLoginBinding        loginBinding;
    private FirebaseAuth                firebaseAuth;
    HomeFragment                        homeFragment;

    AiocaNetworkService                 aiocaNetworkService;

    private ServiceHandle               serviceHandle;
    private boolean                     isBound                 = false;

    private boolean                     finishedGettingSettings = false;


    public LoginFragment() {
    }

    /**
     * Firebase Authentication Instance used for making the Authentication of the User
     * @param savedInstanceState Bundle
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.serviceHandle = ServiceHandle.getInstance(Objects.requireNonNull(getActivity()).getApplicationContext());
        firebaseAuth = FirebaseAuth.getInstance();
    }

    /**
     * Creating the databinding and setting its Viewmodel
     * @param inflater LayoutInflater
     * @param container ViewGroup
     * @param savedInstanceState Bundle
     * @return View
     */
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        loginBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false);
        LoginViewModel loginViewModel = new ViewModelProvider(this).get(String.valueOf(R.string.Login_ViewModel), LoginViewModel.class);
        loginBinding.setLoginViewModel(loginViewModel);
        ActivityCompat.requestPermissions(Objects.requireNonNull(getActivity()), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);

        /**
         * Bind Login to the service!
          */

        if (!serviceHandle.bindToService(serviceConn)){
            Log.d(TAG, "onCreateView: Cannot bind to Service");
        }

        /**
         * Add On change Listeners to Text
         */
         focusListenerOnText();

        /**
         * SetOnClickListener for When user Presses Login
         */
        loginBinding.loginButton.setOnClickListener(view -> {
            /**
             * Will Parse the Email and Password from the textfied and Try to Login on the Firebase
             * Will also hide the buttons
             */
            if (aiocaNetworkService != null){
                String tempEmail_ = Objects.requireNonNull(
                        loginBinding.loginTextInputEditTextUsername.getText())
                        .toString().trim();
                String tempPass_ = Objects.requireNonNull(
                        loginBinding.loginTextInputEditTextPassword.getText())
                        .toString()
                        .trim();

                if(tempPass_.length() < 8){
                    /**
                     * Simple Character check in password
                     */
                    Snackbar.make(Objects.requireNonNull(getView()),
                            "Password has to be min 8 characters",
                            Snackbar.LENGTH_SHORT)
                            .show();
                }else {
                    loginUserOnFirebase(tempEmail_, tempPass_);
                    hideUserCheckButtons();
                }
            }else{
                Snackbar.make(Objects.requireNonNull(getView()),
                        "Not Connected to Service", Snackbar.LENGTH_LONG)
                        .show();
            }
        });

        return loginBinding.getRoot();
    }

    ServiceConnection serviceConn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {

            aiocaNetworkService = ((AiocaNetworkService.AiocaNetworkServiceBinder) iBinder).getService();
            isBound = true;
            Log.d(TAG, "onServiceConnected: Connected to AiocaNetworkService");
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {

            aiocaNetworkService = null;
            isBound = false;
            Log.d(TAG, "onServiceDisconnected: Disconnected from AiocaNetworkService");
        }
    };

    /**
     * onPause, Unbind from Service
     */
    @Override
    public void onPause() {
        if (isBound){
            Log.d(TAG, "onPause: Unbinding from Service");
            serviceHandle.unbindToService(serviceConn);
        }
        isBound = false;
        super.onPause();
    }

    /**
     * onResume, will rebind to service
     */
    @Override
    public void onResume() {
        if (!isBound){
            Log.d(TAG, "onResume: Binding to Service");

            if (!serviceHandle.bindToService(serviceConn)){
                Snackbar.make(Objects.requireNonNull(getView()),
                        "Cannot Connect to Service in onResume", Snackbar.LENGTH_LONG)
                        .show();
            }
        }
        super.onResume();
    }

    /**
     * Method for Launching the Home Fragment after Login is complete
     */
    private void launchHomeFragment(){
        Log.d(TAG, "launchHomeFragment: Launching Home Fragment");

        homeFragment = (HomeFragment) Objects.requireNonNull(
                getActivity())
                .getSupportFragmentManager()
                .findFragmentByTag(String.valueOf(R.string.HomeFragment_Id));
        if (homeFragment == null){
            homeFragment = new HomeFragment();
        }

        loginBinding.loginLoading.setVisibility(View.GONE);

        // Reset when fragment stack changes. Here when new fragment is launched
        setViewOnStackChanges();

        Objects.requireNonNull(((AppCompatActivity) Objects.requireNonNull(getActivity()))
                .getSupportActionBar()).show();

        FragmentTransaction fragmentTransaction = getActivity()
                .getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, homeFragment);
        fragmentTransaction.commit();
    }

    /**
     * Will try to Login the User to Firebase with Emil and Pass
     * If user is not found it will register with Firebase instead
     * @param email_ String, Email
     * @param pass_ String, Password
     */
    public void loginUserOnFirebase(String email_, String pass_){
        firebaseAuth.signInWithEmailAndPassword(email_, pass_).addOnCompleteListener(task -> {
            if (task.isSuccessful() && aiocaNetworkService != null) {

                Log.d(TAG, "loginUserOnFirebase: UserID: " + firebaseAuth.getUid());
                /**
                 * Will log onto the Network
                 */
                aiocaNetworkService.wiFiController.registerToNetwork();

                /**
                 * Will Wait until network is connected and then launch home fragment
                 */
                startThreadToWaitForConnection();

            } else {

                if (aiocaNetworkService == null) {
                    Snackbar.make(Objects.requireNonNull(getView()),
                            "Connection to Service not Available",
                            Snackbar.LENGTH_SHORT)
                            .show();
                } else {
                    String errorCode;
                    try {
                        errorCode = ((FirebaseAuthException) Objects.requireNonNull(task.getException())).getErrorCode();
                    } catch (Exception e) {
                        errorCode = "ERROR";
                    }
                    showUserCheckButtons();
                    switch (errorCode) {

                        case "ERROR_WRONG_PASSWORD":
                            Snackbar.make(Objects.requireNonNull(getView()),
                                    "Password was not correct",
                                    Snackbar.LENGTH_SHORT)
                                    .show();
                            break;

                        case "ERROR_USER_NOT_FOUND":
                            Snackbar.make(Objects.requireNonNull(getView()),
                                    "User not found! Want to Register?",
                                    Snackbar.LENGTH_LONG)
                                    .setAction("Register", view1 -> registerUserOnFirebase(email_, pass_))
                                    .show();
                            break;

                        default:
                            Snackbar.make(Objects.requireNonNull(getView()),
                                    Objects.requireNonNull(task.getException()).getMessage(),
                                    Snackbar.LENGTH_SHORT)
                                    .show();
                            break;
                    }
                }
            }
        });

    }

    /**
     * Will Register user on Firebase
     * @param email_ String, Email
     * @param pass_ String, Password
     */
    public void registerUserOnFirebase(String email_, String pass_){
        firebaseAuth.createUserWithEmailAndPassword(
                email_,
                pass_)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful() && aiocaNetworkService != null){

                        Log.d(TAG, "loginUserOnFirebase: UserID: " + firebaseAuth.getUid());

                        /**
                         * Register to Network if create user is successful
                         * else report the error for the user
                         */
                        aiocaNetworkService.wiFiController.registerToNetwork();
                        startThreadToWaitForConnection();

                    }else {
                        if (aiocaNetworkService == null) {
                            Snackbar.make(Objects.requireNonNull(getView()),
                                    "Connection to Service not Available",
                                    Snackbar.LENGTH_SHORT)
                                    .show();
                        } else {

                            String errorCode;
                            try {
                                errorCode = ((FirebaseAuthException) Objects.requireNonNull(task.getException())).getErrorCode();
                            } catch (Exception e) {
                                errorCode = "ERROR";
                            }
                            showUserCheckButtons();
                            switch (errorCode) {
                                case "ERROR_EMAIL_ALREADY_IN_USE":
                                    Snackbar.make(Objects.requireNonNull(getView()),
                                            "User Already Exists",
                                            Snackbar.LENGTH_SHORT)
                                            .show();
                                    break;
                                default:
                                    Snackbar.make(Objects.requireNonNull(getView()),
                                            Objects.requireNonNull(task.getException()).getMessage(),
                                            Snackbar.LENGTH_SHORT)
                                            .show();
                                    break;
                            }
                        }
                    }
                });
    }

    /**
     * For Reseting the views when the stack of the fragments is changing. EG launching new
     * Fragment. Will remove itself after its been run once!
     */
    private void setViewOnStackChanges(){
        getParentFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                showUserCheckButtons();
                /**
                 * Remove Listener when BackStack Has changed once and UI has been reset
                 */
                getParentFragmentManager().removeOnBackStackChangedListener(this);
            }
        });
    }

    /**
     * Start Thread that will start loading icon and check if connection is made..
     * if not then timeout after some time
     * Maybe start loading icon after this and remove it in onAvailable,
     * OnLost and OnUnavailable can show it as well
     */
    private void startThreadToWaitForConnection(){
        int MillisecondsToWait = 20000;
        int sleepTimeMs = 100;

        hideUserCheckButtons();

        new Thread(() -> {
            int i = 0;
            try {
                while (i < (MillisecondsToWait / sleepTimeMs)) {
                    if (aiocaNetworkService.wiFiController.isConnected()) {
                        break;
                    }
                    i = i + 1;
                    Thread.sleep(sleepTimeMs);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (getActivity() != null) {
                Objects.requireNonNull(getActivity()).runOnUiThread(() -> {
                if (aiocaNetworkService.wiFiController.isConnected()) {
                    try {
                        /**
                         * Send the User ID to the Main Ctrl.
                         */
                        SendUserIDObserver.CallBackInterface cb = new SendUserIDObserver.CallBackInterface() {
                            @Override
                            public void CallBackSuccess(boolean loggedIn) {
                                userAuthorizationCallback(loggedIn);
                            }

                            @Override
                            public void CallBackError() {
                                networkErrorCallback();
                            }
                        };

                        aiocaNetworkService.sendUserID(
                                new SendUserIDObserver(cb),
                                Objects.requireNonNull(firebaseAuth.getUid()));
                    }
                    catch (IOException e)
                    {
                        Log.d(TAG, "startThreadToWaitForConnection: ", e);
                    }

                } else {
                    /**
                     * If the connection to the Wifi hotspot times out
                     */
                    networkErrorCallback();
                }
            });
        }
        }).start();

        hideUserCheckButtons();
    }

    /**
     * Simple Dialog Box that shows App cant connect to WiFi
     */
    private void ErrorLogin(){
        new MaterialAlertDialogBuilder(Objects.requireNonNull(getContext()))
                .setTitle(getString(R.string.LoginErrorTitle))
                .setMessage(getString(R.string.LoginErrorMessage))
                .show();
    }

    /**
     * Callback to run when a user has been Authorized on MainCtrl
     * @param loggedIn Boolean, Whether logged in or not
     */
    private void userAuthorizationCallback(boolean loggedIn){
        if (loggedIn) {

            // Vector is Thread Safe and therefore its used
            ConcurrentLinkedQueue<AiocaSettings.UserSettings> concurrentLinkedQueue = new ConcurrentLinkedQueue<>();


            SettingsViewModel settingsViewModel = new ViewModelProvider(this).
                    get(String.valueOf(R.string.Settings_ViewModel), SettingsViewModel.class);

            concurrentLinkedQueue.add(AiocaSettings.UserSettings.parkingCamera);
            concurrentLinkedQueue.add(AiocaSettings.UserSettings.roadStripAlert);
            concurrentLinkedQueue.add(AiocaSettings.UserSettings.suddenBrake);

            aiocaNetworkService.receiveUserSettings(new CameraSettingsObserver(settingsViewModel, concurrentLinkedQueue), AiocaSettings.UserSettings.parkingCamera);
            aiocaNetworkService.receiveUserSettings(new RoadStripAlertSettingsObserver(settingsViewModel, concurrentLinkedQueue), AiocaSettings.UserSettings.roadStripAlert);
            aiocaNetworkService.receiveUserSettings(new SuddenBrakeSettingsObserver(settingsViewModel, concurrentLinkedQueue), AiocaSettings.UserSettings.suddenBrake);


            new Thread(() -> {
                finishedGettingSettings = true;
                int timeout = 0;
                while (concurrentLinkedQueue.size() > 0){
                    try {
                        Thread.sleep(500);
                        timeout += 1;
                        if (timeout > 20){
                            finishedGettingSettings = false;
                            break;
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                if (getActivity() != null) {
                    Objects.requireNonNull(getActivity()).runOnUiThread(() -> {
                        if (finishedGettingSettings){
                            setViewOnStackChanges();
                            launchHomeFragment();
                        }else {
                            showUserCheckButtons();
                            ErrorLogin();
                        }
                    });
                }
            }).start();
        }
        else {
            showUserCheckButtons();
            ErrorLogin();
        }
    }

    /**
     * Will reshow button and show error message for user
     */
    private void networkErrorCallback(){
        showUserCheckButtons();
        openNoNetworkButton();
    }

    /**
     * Simple Dialog Box that shows App cant connect to WiFi
     */
    private void openNoNetworkButton(){
        new MaterialAlertDialogBuilder(Objects.requireNonNull(getContext()))
                .setTitle(getString(R.string.noNetworkTitle))
                .setMessage(getString(R.string.noNetworkMessage))
                .setPositiveButton(getString(R.string.noNetworkGoToSettings), (dialog, which) -> {
                    Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    getContext().startActivity(intent);
                })
                .setNegativeButton(getString(R.string.noNetworkCloseApp), (dialog, which) ->
                        System.exit(0))
                .show();
    }

    /**
     * Will toggle the Password and Username to be visible
     */
    private void showUserCheckButtons(){
        loginBinding.loginLoading.setVisibility(View.GONE);

        if (loginBinding.loginTextInputLayoutUsername.getVisibility() == View.GONE){
            loginBinding.loginTextInputLayoutUsername.setVisibility(View.VISIBLE);
        }

        if (loginBinding.loginTextInputLayoutPassword.getVisibility() == View.GONE){
            loginBinding.loginTextInputLayoutUsername.setVisibility(View.VISIBLE);
        }

        if (loginBinding.loginButton.getVisibility() == View.GONE){
            loginBinding.loginButton.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Will Hide the Username, Password and LoginButton
     */
    private void hideUserCheckButtons(){
        loginBinding.loginLoading.setVisibility(View.VISIBLE);

        if (loginBinding.loginTextInputLayoutUsername.getVisibility() == View.VISIBLE){
            loginBinding.loginTextInputLayoutUsername.setVisibility(View.GONE);
        }

        if (loginBinding.loginTextInputLayoutPassword.getVisibility() == View.VISIBLE){
            loginBinding.loginTextInputLayoutUsername.setVisibility(View.GONE);
        }

        if (loginBinding.loginButton.getVisibility() == View.VISIBLE){
            loginBinding.loginButton.setVisibility(View.GONE);
        }
    }

    /**
     * Will add and TextWatcher to the Password and Email field to check on text changes and
     * present user if this is not valid inputs
     */
    private void focusListenerOnText(){
        loginBinding.loginTextInputEditTextPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (Objects.requireNonNull(loginBinding.loginTextInputEditTextPassword.getText()).toString().length() < 8
                        && Objects.requireNonNull(loginBinding.loginTextInputEditTextPassword.getText()).toString().length() > 0){
                    loginBinding.loginTextInputLayoutPassword.setError("Password has to be minimum 8 long");
                }else{
                    loginBinding.loginTextInputLayoutPassword.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
        loginBinding.loginTextInputEditTextUsername.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!Patterns.EMAIL_ADDRESS.matcher(Objects.requireNonNull(loginBinding.loginTextInputEditTextUsername.getText()).toString().trim()).matches()
                        && loginBinding.loginTextInputEditTextUsername.length() > 0){
                    loginBinding.loginTextInputLayoutUsername.setError("Has to be an usable Email");
                }else{
                    loginBinding.loginTextInputLayoutUsername.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }
}