package aioca.example.aiocaapplication.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Objects;

import aioca.example.aiocaapplication.R;
import aioca.example.aiocaapplication.util.ServiceHandle;

public class HomeFragment extends Fragment {

    private ServiceHandle serviceHandle;

    public HomeFragment() {
    }


    /**
     * onCreate of the LoginFragment, Will instantiate serviceHandle
     * @param savedInstanceState Bundle
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.serviceHandle = ServiceHandle.getInstance(Objects.requireNonNull(getActivity()).getApplicationContext());
    }

    /**
     * Creating the databinding and setting its Viewmodel
     * @param inflater LayoutInflater
     * @param container ViewGroup
     * @param savedInstanceState Bundle
     * @return View
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }
}