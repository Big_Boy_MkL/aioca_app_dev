package aioca.example.aiocaapplication.fragments;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.os.IBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Objects;

import aioca.example.aiocaapplication.R;
import aioca.example.aiocaapplication.services.AiocaNetworkService;
import aioca.example.aiocaapplication.databinding.FragmentCameraBinding;
import aioca.example.aiocaapplication.networking.observers.StartVideoObserver;
import aioca.example.aiocaapplication.networking.observers.VideoObserver;
import aioca.example.aiocaapplication.util.ServiceHandle;
import aioca.example.aiocaapplication.viewmodels.CameraViewModel;

public class CameraFragment extends Fragment {

    private static String                   TAG = "CameraFragment";


    private FragmentCameraBinding           cameraBinding;
    private AiocaNetworkService             aiocaNetworkService;
    private boolean                         isBound = false;

    private ServiceHandle                   serviceHandle;

    StartVideoObserver                      startVideoObserver;
    VideoObserver                           videoObserver;

    public CameraFragment() {
    }

    /**
     * onCreate of the CameraFragment, Will instantiate serviceHandle
     * @param savedInstanceState Bundle
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.serviceHandle = ServiceHandle.getInstance(Objects.requireNonNull(getActivity()).getApplicationContext());
    }

    /**
     * onResume will bind Bind to Service
     */
    @Override
    public void onResume() {
        Log.d(TAG, "onResume: ");

        if (!serviceHandle.bindToService(ServiceConn)){
            Log.d(TAG, "onCreateView: Cannot bind to Service");
        }

        super.onResume();
    }

    /**
     * onPause, Will shutdown keepalive and threads is active aswell as unbind from the service
     */
    @Override
    public void onPause() {
        Log.d(TAG, "onPause: ");

        if (isBound)
        {
            /**
             * Will Make sure to Close down Keep Alive Executor and UDP Stream Thread
             */
            aiocaNetworkService.udpObservables.shutdownKeepAlive();

            aiocaNetworkService.udpObservables.setThreadForClose();
        }

        /**
         * Unbind to Service as we are exiting the Fragment
         */
        serviceHandle.unbindToService(ServiceConn);

        super.onPause();
    }

    /**
     * onCreateView, Will Inflate the view aswell as create a viewmodel and configure the
     * databinding
     * @param inflater LayoutInflater
     * @param container ViewGroup
     * @param savedInstanceState Bundle
     * @return View to show
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        cameraBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_camera, container, false);
        CameraViewModel cameraViewModel = new ViewModelProvider(this).get(String.valueOf(R.string.Camera_ViewModel), CameraViewModel.class);
        cameraBinding.setCameraViewModel(cameraViewModel);

        return cameraBinding.getRoot();
    }

    /**
     * onDestory
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
    }



    ServiceConnection ServiceConn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {

            /**
             * Get Binding for Service to have a Handle to it!
             */
            isBound = true;
            aiocaNetworkService = ((AiocaNetworkService.AiocaNetworkServiceBinder) iBinder).getService();

            /**
             * Make an Video Observer and assign this to the Services VideoObserver
             */
            videoObserver = new VideoObserver(cameraBinding.cameraVideoView,
                    cameraBinding.fpsCounterTextview, getActivity());
            aiocaNetworkService.setVideoObserver(videoObserver);

            /**
             * Making a Start Video Feed Observer which will start UDP feed when successful
             */
            startVideoObserver = new StartVideoObserver(aiocaNetworkService, getContext());
            aiocaNetworkService.startVideoFeed(startVideoObserver);


            Log.d(TAG, "onServiceConnected: Connected to AiocaNetworkService");
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            /**
             * Could Maybe have the close threads and executor
             * Thought there have been seen cases on exit where onServiceDisconnected is called
             * alot later then Disconnect from WiFi
             */
            isBound = false;
            Log.d(TAG, "onServiceDisconnected: Disconnected from AiocaNetworkService");

        }
    };
}