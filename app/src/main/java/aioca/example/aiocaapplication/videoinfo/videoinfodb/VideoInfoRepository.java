package aioca.example.aiocaapplication.videoinfo.videoinfodb;

import android.app.Application;

import androidx.lifecycle.LiveData;

import java.util.List;

import aioca.example.aiocaapplication.videoinfo.executors.InsertVideoInfoExecutor;
import aioca.example.aiocaapplication.videoinfo.models.VideoInfo;

/**
 * Inspiration taken from
 * https://developer.android.com/codelabs/android-training-livedata-viewmodel#14
 */

public class VideoInfoRepository {


    private DaoVideoInfoInterface daoVideoInfoInterface;
    LiveData<List<VideoInfo>> videoInfoLiveData;
    InsertVideoInfoExecutor insertVideoInfoExecutor;


    /**
     * Constructor for creating repository
     * @param application Application context
     */
    public VideoInfoRepository(Application application) {

        VideoInfoDatabase db = VideoInfoDatabase.getInstance(application);
        daoVideoInfoInterface = db.daoVideoInfoInterface();
        videoInfoLiveData = daoVideoInfoInterface.getAllVideoInfoLive();
        insertVideoInfoExecutor = new InsertVideoInfoExecutor(daoVideoInfoInterface);
    }


    /**
     * @return Receive livedata object
     */
    public LiveData<List<VideoInfo>> getVideoInfoLiveData() {return videoInfoLiveData; }

    /**
     * @param videoInfo VideoInfo Element to add to database
     */
    public void insert(VideoInfo videoInfo){
        insertVideoInfoExecutor.insertVideoInfo(videoInfo);
    }

}
