package aioca.example.aiocaapplication.videoinfo.executors;

import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import aioca.example.aiocaapplication.videoinfo.models.VideoInfo;
import aioca.example.aiocaapplication.videoinfo.videoinfodb.DaoVideoInfoInterface;

/**
 * Class for inserting a VideoInfo Object into the Room Database
 */
public class InsertVideoInfoExecutor {

    DaoVideoInfoInterface daoVideoInfoInterface;

    private final ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(2,
            2, 0,
            TimeUnit.MILLISECONDS, new LinkedBlockingDeque<>());

    public InsertVideoInfoExecutor(DaoVideoInfoInterface daoVideoInfoInterface){
        this.daoVideoInfoInterface = daoVideoInfoInterface;
    }

    public void insertVideoInfo(VideoInfo videoInfo){
        threadPoolExecutor.execute(getInsertRunnable(videoInfo));
    }

    private Runnable getInsertRunnable(VideoInfo videoInfo){
        return () -> daoVideoInfoInterface.insertVideoInfo(videoInfo);
    }


}
