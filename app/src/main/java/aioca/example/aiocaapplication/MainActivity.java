package aioca.example.aiocaapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.MenuItem;

import com.google.android.material.navigation.NavigationView;

import java.util.Objects;

import aioca.example.aiocaapplication.networking.sharedpreferences_handlers.NetworkIdSharedPreferences;
import aioca.example.aiocaapplication.services.AiocaNetworkService;
import aioca.example.aiocaapplication.fragments.CameraFragment;
import aioca.example.aiocaapplication.fragments.HomeFragment;
import aioca.example.aiocaapplication.fragments.LoginFragment;
import aioca.example.aiocaapplication.fragments.SavedFragment;
import aioca.example.aiocaapplication.fragments.SettingsFragment;
import aioca.example.aiocaapplication.util.ServiceHandle;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private final static String     TAG                     = "MainActivity";
    private DrawerLayout            drawerLayout;
    boolean                         loggedIn                = false;

    private CameraFragment          cameraFragment;
    private HomeFragment            homeFragment;
    private SavedFragment           savedFragment;
    private SettingsFragment        settingsFragment;

    private ServiceHandle               serviceHandle;
    private NetworkIdSharedPreferences  networkIdSharedPreferences;
    private AiocaNetworkService         aiocaNetworkService;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /**
         * Singleton to connect to Service given to all Fragments
         * that need connection to the AiocaService
         */
        serviceHandle = ServiceHandle.getInstance(this);
        networkIdSharedPreferences = NetworkIdSharedPreferences.getInstance(this);

        /**
         * We are starting the service so that it doesn't close when Activity unbinds from service
         * This is so that service should only be killed on Activity onDestroy and since
         * MainActivity never needs a bind we can wait for Activity to unBind from service
         */
        Intent serviceIntent = new Intent(this, AiocaNetworkService.class);
        startService(serviceIntent);

        setupDrawer();

        setupFragments(savedInstanceState);
    }

    @Override
    protected void onResume() {
        Log.d(TAG, "onResume: Called");
        /**
         * Should only reconnect to WiFi and then remake the sockets in the service
         */
        if (aiocaNetworkService == null) {
            serviceHandle.bindToService(serviceConn);
        }
        // a quick connect to Service can happen before connectoToWiFiFromPause will happen
        if (aiocaNetworkService != null){
            aiocaNetworkService.wiFiController.connectToWiFiFromPause();
        }
        super.onResume();
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "onPause: Called");
        /**
         * Should prob only disconnect from WiFi and then Fragments onPause should be unbinding from service
         */

        super.onPause();

        if (aiocaNetworkService != null){

            aiocaNetworkService.wiFiController.disconnectFromWiFiOnPause();
            serviceHandle.unbindToService(serviceConn);
        }

    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy: Called");
        /**
         * OnDestroy should only stop the running service
         * and delete shared preferences
         */

        /**
         * Will Clear the stored NetworkId stored in Shared Preferences
         */
        networkIdSharedPreferences.clearNetworkIdSharedPreferences();

        Intent AiocaServiceIntent = new Intent(this, AiocaNetworkService.class);
        stopService(AiocaServiceIntent);

        super.onDestroy();

    }

    ServiceConnection serviceConn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {

            aiocaNetworkService = ((AiocaNetworkService.AiocaNetworkServiceBinder)iBinder).getService();
            Log.d(TAG, "onServiceConnected: Connected to AiocaNetworkService");

        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {

            aiocaNetworkService = null;
            Log.d(TAG, "onServiceDisconnected: Disconnected from AiocaNetworkService");

        }
    };

    /**
     *
     * Setting fragment and id to the selected item in Sidebar and starting that fragment
     * Inspiration from
     * https://stackoverflow.com/questions/32944798/switch-between-fragments-with-onnavigationitemselected-in-new-navigation-drawer
     * @author Mikkel Jensen
     * @param item Menu item clicked on
     * @return a Boolean that operation is successful
     * @IllegalStateException Will throw if an item that is not in the list i pressed
     */
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        Fragment fragment;
        int id_;

        switch (item.getItemId()){
            case R.id.home_fragment_layout:
                fragment = homeFragment;
                id_ = R.string.HomeFragment_Id;
                break;
            case R.id.camera_fragment_layout:
                fragment = cameraFragment;
                id_ = R.string.CameraFragment_Id;
                break;
            case R.id.saved_fragment_layout:
                fragment = savedFragment;
                id_ = R.string.SavedFragment_Id;
                break;
            case R.id.settings:
                fragment = settingsFragment;
                id_ = R.string.SettingsFragment_Id;
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + item.getItemId());
        }
        if (fragment != null){
            fragmentTransaction.replace(R.id.fragment_container, fragment, String.valueOf(id_));
            fragmentTransaction.commit();
        }
        if (drawerLayout.isDrawerOpen(GravityCompat.START)){
            drawerLayout.closeDrawer(GravityCompat.START);
        }
        return true;
    }

    /**
     * Creating new fragments if application is started from new else retrieve old fragments and
     * start the login fragment
     * The implementation is inspired from the SMAP Course on AU, the example is developed by
     * the teachers of the course.
     * @param savedInstanceState Bundle of earlier state of problem. Eg on Rotation will be null
     *                           if app is started from new
     */
    private void setupFragments(Bundle savedInstanceState){
        LoginFragment loginFragment;
        if (savedInstanceState == null){
            // Make new Fragments
            cameraFragment = new CameraFragment();
            homeFragment = new HomeFragment();
            savedFragment = new SavedFragment();
            settingsFragment = new SettingsFragment();
            loginFragment = new LoginFragment();

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, loginFragment, String.valueOf(R.string.UserCheckFragment_Id))
                    .commit();
        }else{
                cameraFragment = (CameraFragment)getSupportFragmentManager()
                        .findFragmentByTag(String.valueOf(R.string.CameraFragment_Id));
                if (cameraFragment == null){
                    cameraFragment = new CameraFragment();
                }

                homeFragment = (HomeFragment)getSupportFragmentManager()
                        .findFragmentByTag(String.valueOf(R.string.HomeFragment_Id));
                if (homeFragment == null){
                    homeFragment = new HomeFragment();
                }

                savedFragment = (SavedFragment)getSupportFragmentManager()
                        .findFragmentByTag(String.valueOf(R.string.SavedFragment_Id));
                if (savedFragment == null){
                    savedFragment = new SavedFragment();
                }

                settingsFragment = (SettingsFragment)getSupportFragmentManager()
                        .findFragmentByTag(String.valueOf(R.string.SettingsFragment_Id));
                if (settingsFragment == null){
                    settingsFragment = new SettingsFragment();
                }

                loginFragment = (LoginFragment) getSupportFragmentManager()
                        .findFragmentByTag(String.valueOf(R.string.LoginFragment_Id));
                if (loginFragment == null){
                    loginFragment = new LoginFragment();
                }
        }

        /**
         * If no login is registered, start Login page instead
         * TODO Still not Implemented.. Should be done using Firebase
         * Remember Firebase has Offline Implementation that could be used
         */
        if (!loggedIn){
            setDrawer(true);
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, loginFragment);
            fragmentTransaction.commit();
        }else {
            setDrawer(false);

            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, homeFragment);
            fragmentTransaction.commit();
        }
    }

    /**
     * Setting up the application drawer so we can have the sidebar
     */
    private void setupDrawer(){
        Toolbar toolbar = findViewById(R.id.main_toolbar);
        drawerLayout = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.menu_view);
        setSupportActionBar(toolbar);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(
                this,
                drawerLayout,
                toolbar,
                R.string.open_nav_draw,
                R.string.close_nav_draw
        );
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);
    }

    /**
     * Will change if the Drawer is locked or not
     * Commented Code is there since they might be needed.
     * Drawer might still be able to be opened by drag if not locked
     * @param enabled Whether to Show or Disable Drawer
     */
    public void setDrawer(boolean enabled){
        if (enabled){
            //toolbar.setNavigationIcon(null);
            Objects.requireNonNull(getSupportActionBar()).hide();
            //drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        }else {
            Objects.requireNonNull(getSupportActionBar()).show();
            //drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        }
    }

}
