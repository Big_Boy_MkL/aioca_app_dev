package aioca.example.aiocaapplication.videoencoder;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.WritableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import org.jcodec.api.SequenceEncoder;
import org.jcodec.api.android.AndroidSequenceEncoder;
import org.jcodec.common.Codec;
import org.jcodec.common.Format;
import org.jcodec.common.io.NIOUtils;
import org.jcodec.common.io.SeekableByteChannel;
import org.jcodec.common.model.ColorSpace;
import org.jcodec.common.model.Picture;
import org.jcodec.common.model.Rational;
import org.jcodec.common.model.Rect;

public class InternalDataStorage {

    private String TAG = "InternalDataStorage";
    private Context context;
    private String path;

    public InternalDataStorage(Context context){
        this.context = context;
        path = context.getFilesDir().getPath();

        Log.d(TAG, "InternalDataStorage: path is: " + path);
    }

    /**
     * WriteFrameToInternalStorage, Writes data to the internal
     * app storage. Each frame will be saved in a folder containing
     * the video number the filename will contain the frame number.
     * @param data Data to be stored.
     * @param videoNumber Name for the folder containing the frame.
     * @param frameNumber The frame number of the frame being stored.
     */
    public void WriteFrameToInternalStorage(byte[] data, int videoNumber, int frameNumber){
        String child = "Video_" + videoNumber + "/frame_" + frameNumber;

        // Create directory
        new File(path, "Video_" + videoNumber).mkdir();

        // Create file
        File file = new File(path, child);

        // Create output stream and write data to file
        try {
            FileOutputStream outputStream = new FileOutputStream(file);
            outputStream.write(data);
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * ReadFrameToInternalStorage, Reads the stored frame from a specific
     * video number.
     * @param videoNumber Name for the folder containing the frame.
     * @param frameNumber The number of the specific frame to be retrieved.
     * @return byte[]
     */
    public byte[] ReadFrameToInternalStorage(int videoNumber, int frameNumber){
        String child = "Video_" + videoNumber + "/frame_" + frameNumber;
        File file = new File(path, child);
        byte[] data = new byte[(int)file.length()];

        try {
            FileInputStream inputStream = new FileInputStream(file);
            inputStream.read(data, 0, data.length);
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }

    /**
     * DeleteStoredVideoInfo, deletes all frames in the folder
     * @param videoNumber Name for the folder containing the video frames.
     */
    public void DeleteStoredVideoInfo(int videoNumber){
        File[] files = new File(path, "Video_" + videoNumber + "/").listFiles();

        for (int i = 0; i < files.length; i++) {
            if (files[i].exists())
                files[i].delete();
        }
    }

    /**
     * NumberOfStoredFrames, Checks the number of saved frames for a
     * specific video.
     * will setup recyclerview and databinding
     * @param videoNumber Name for the folder containing the video frames.
     * @return int
     */
    public int NumberOfStoredFrames(int videoNumber) {
        File directory = new File(path, "Video_" + videoNumber + "/");
        if (directory.exists())
            return directory.listFiles().length;
        else
            return 0;
    }

    /**
     * VideoExists, Checks if the video with the specific video number
     * already exists in the download folder,
     * @param videoNumber Number for video to check if exists.
     * @return boolean
     */
    public boolean VideoExists(int videoNumber){
        File dir = new File("//sdcard//Download//");
        File file = new File(dir, "Video" + videoNumber + ".mp4");
        return file.exists();
    }

    /**
     * GetPath, returns the path to where the encoded video should be stored,
     * @param videoNumber Number for video to get path to
     * @return File
     */
    public File GetPath(int videoNumber){
        if (VideoExists(videoNumber)){
            File dir = new File("//sdcard//Download//");
            return new File(dir, "Video" + videoNumber + ".mp4");
        }
        else {
            return null;
        }
    }
}
