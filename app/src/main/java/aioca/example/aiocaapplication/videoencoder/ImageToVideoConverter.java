package aioca.example.aiocaapplication.videoencoder;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;

import org.jcodec.api.android.AndroidSequenceEncoder;
import org.jcodec.common.io.NIOUtils;
import org.jcodec.common.io.SeekableByteChannel;
import org.jcodec.common.model.Picture;
import org.jcodec.common.model.Rational;
import org.jcodec.scale.BitmapUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class ImageToVideoConverter {
    InternalDataStorage storage;
    int FPS = 10;
    SeekableByteChannel out = null;
    AndroidSequenceEncoder encoder = null;

    public ImageToVideoConverter(InternalDataStorage storage){
        this.storage = storage;
    }

    /**
     * ConvertFramesIntoVideo, will convert downloaded video framed
     * into a mp4 video and store it in the Download folder.
     * @param videoNumber number of downloaded video
     * @param numOfFrames number of frames that needs to be converted
     */
    public void ConvertFramesIntoVideo(int videoNumber, int numOfFrames) throws IOException {
        int numberOfFrames = storage.NumberOfStoredFrames(videoNumber);

        if (numberOfFrames > 0){

            // Create file in Download directory
            File dir = new File("//sdcard//Download//");
            File file = new File(dir, "Video" + videoNumber + ".mp4");
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }

            // Create writeableFileChannel for encoder
            try {
                out =  NIOUtils.writableFileChannel(file.getPath());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Log.e("msg",e.toString());
            }

            // Create encoder
            try {
                encoder = new AndroidSequenceEncoder(out, Rational.R(FPS,numOfFrames/ FPS));
            } catch (IOException e) {
                e.printStackTrace();
            }

            for (int i = 0; i < numberOfFrames; i++) {
                // Read frame
                byte[] storedImage = storage.ReadFrameToInternalStorage(videoNumber, i);

                // Convert to bitmap and add to encode list
                Bitmap bitmap = BitmapFactory.decodeByteArray(storedImage, 0, storedImage.length);
                Picture pic = BitmapUtil.fromBitmap(bitmap);
                encoder.encodeNativeFrame(pic);
            }

            // Do the encoding
            encoder.finish();
        }
    }
}
