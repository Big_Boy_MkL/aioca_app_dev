package aioca.example.aiocaapplication.settings.settingsdb;

import android.content.Context;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import aioca.example.aiocaapplication.R;
import aioca.example.aiocaapplication.settings.SettingsClass;


/**
 * https://developer.android.com/topic/libraries/architecture/room
 */
@Database(entities = SettingsClass.class, exportSchema = false, version = 8)
public abstract class SettingsDatabase extends RoomDatabase {

    private static SettingsDatabase db;


    /**
     * Singleton for getting the Database
     * @param context Context
     * @return SettingsDatabase
     */
    public static synchronized SettingsDatabase getInstance(Context context){
        if (db == null)
        {
            db = Room.databaseBuilder(context.getApplicationContext(), SettingsDatabase.class,
                    context.getString(R.string.settings_db_string))
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return db;
    }

    /**
     * Dao
     * @return Dao
     */
    public abstract DaoSettingsInterface daoSettingsInterface();
}
