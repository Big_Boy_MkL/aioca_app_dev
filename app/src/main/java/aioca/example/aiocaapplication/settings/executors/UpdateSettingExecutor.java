package aioca.example.aiocaapplication.settings.executors;


import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import aioca.example.aiocaapplication.settings.SettingsClass;
import aioca.example.aiocaapplication.settings.settingsdb.DaoSettingsInterface;

/**
 * https://docs.oracle.com/javase/7/docs/api/java/util/concurrent/ThreadPoolExecutor.html
 */
public class UpdateSettingExecutor {

    DaoSettingsInterface daoSettingsInterface;

    private final ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(2,
            2,0,
            TimeUnit.MILLISECONDS, new LinkedBlockingDeque<>());

    /**
     * Constructor Executor
     * @param daoSettingsInterface Dao
     */
    public UpdateSettingExecutor(DaoSettingsInterface daoSettingsInterface) {

        this.daoSettingsInterface = daoSettingsInterface;

    }

    /**
     * Update the Setting
     * @param settingsClass SettingsClass
     */
    public void UpdateSetting(SettingsClass settingsClass)
    {
        threadPoolExecutor.execute(getUpdateRunnable(settingsClass));
    }

    /**
     * Get Runnable to update database
     * @param settingsClass SettingsClass
     * @return Runnable
     */
    private Runnable getUpdateRunnable(final SettingsClass settingsClass){
        return () -> daoSettingsInterface.update(settingsClass);
    }

}
