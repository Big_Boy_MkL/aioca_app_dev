package aioca.example.aiocaapplication.util;

public class MyConversions {

    /**
     * Simple conversion from Signed byte to Unsigned
     * @param b byte: byte to convert
     * @return int
     */
    public static int convertSignedByteToUnsignedInt(byte b){
        return b & 0xff;
    }

    /**
     * Will decode the supplied header,
     * TODO Maybe make check on size of byte
     * @param b byte[]: byte array to decode image size from
     * @return int: image size
     */
    public static int getImageSizeFromByteArrayHeader(byte[] b){
        int bufimagesize = 0;
        bufimagesize += (convertSignedByteToUnsignedInt(b[2]) << 24);
        bufimagesize += (convertSignedByteToUnsignedInt(b[3]) << 16);
        bufimagesize += (convertSignedByteToUnsignedInt(b[4]) << 8);
        bufimagesize += (convertSignedByteToUnsignedInt(b[5]));
        return bufimagesize;
    }

}
