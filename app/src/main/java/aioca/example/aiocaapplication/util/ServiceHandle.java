package aioca.example.aiocaapplication.util;

import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;

import java.util.Objects;

import aioca.example.aiocaapplication.services.AiocaNetworkService;

public class ServiceHandle {


    private static ServiceHandle INSTANCE = null;
    private Context context;


    private ServiceHandle(Context context){
        this.context = context;
    }

    /**
     * Get Instance of ServiceHandle
     * @param context Context
     * @return ServiceHandle
     */
    public static ServiceHandle getInstance(Context context){
        if (INSTANCE == null){
            INSTANCE = new ServiceHandle(context);
        }
        return INSTANCE;
    }

    /**
     * Will bind to service
     * @param ServiceConn ServiceConnection
     * @return If success
     */
    public boolean bindToService(ServiceConnection ServiceConn){
        try{
            return context.getApplicationContext().bindService(new Intent(context, AiocaNetworkService.class),
                    ServiceConn, Context.BIND_AUTO_CREATE);
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    /**
     * will Unbind from service
     * @param ServiceConn ServiceConnection
     * @return Boolean of success
     */
    public boolean unbindToService(ServiceConnection ServiceConn){
        // Returns false is successfully unbound from service.
        try {
            context.getApplicationContext().unbindService(ServiceConn);
            return false;
        }catch (Exception e){
            e.printStackTrace();
            return true;
        }
    }

}
