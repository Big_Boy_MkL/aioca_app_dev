package aioca.example.aiocaapplication.util.Argon2Hash;

import com.lambdapioneer.argon2kt.Argon2Kt;
import com.lambdapioneer.argon2kt.Argon2Mode;

import java.security.SecureRandom;

/**
 * https://password-hashing.net/argon2-specs.pdf
 * Argon2 Specification
 * Argon2 binding for Android by
 * Daniel Hugenroth
 * https://github.com/lambdapioneer/argon2kt
 */
public class Argon2Hasher {
    /* Specification recommends 16 bytes for Password Hashing
     * Argon2Mode is set to Argon2_ID since this is a hybrid of making the hashing
     * CPU and Memory intensive
     */
    private static int DEFAULT_SALT_LENGTH_BYTES = 16;
    private Argon2Kt argon2Kt;
    private Argon2Mode Argon2HashMode = Argon2Mode.ARGON2_ID;

    public Argon2Hasher() {
        argon2Kt = new Argon2Kt();
    }

    public String GenerateArgon2HashResultJava(String password){
        return argon2Kt.hash(Argon2HashMode,
                             password.getBytes(),
                             generateSalt(DEFAULT_SALT_LENGTH_BYTES))
                             .encodedOutputAsString();
    }

    public boolean VerifyArgon2Hash(String hash, String password){
        return argon2Kt.verify(Argon2HashMode, hash, password.getBytes());
    }

    /*
     * https://docs.oracle.com/javase/8/docs/api/java/security/SecureRandom.html
     */
    private byte[] generateSalt(int length_in_bytes){
        SecureRandom secureRandom = new SecureRandom();
        byte[] buf = new byte[length_in_bytes];
        secureRandom.nextBytes(buf);
        return buf;
    }

}
